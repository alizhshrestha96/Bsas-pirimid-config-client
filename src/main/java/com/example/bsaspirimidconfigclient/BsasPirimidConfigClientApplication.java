package com.example.bsaspirimidconfigclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.bus.event.RefreshRemoteApplicationEvent;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.context.scope.refresh.RefreshScopeRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RefreshScope
public class BsasPirimidConfigClientApplication {

    @Value("${msg}")
    private String message;

    public static void main(String[] args) {
        SpringApplication.run(BsasPirimidConfigClientApplication.class, args);
    }

    @GetMapping("/msg")
    public String getMessage() {
        return this.message;
    }

    @EventListener({
            RefreshRemoteApplicationEvent.class,
            ApplicationReadyEvent.class,
            RefreshScopeRefreshedEvent.class
    })
    public void onEvent() {
        System.out.println("new value: " + this.message);
    }

//    @PostMapping("/api/webhook")
//    public ResponseEntity<String> print(@RequestBody String requestBody){
//        System.out.println("#####Webhook####" + requestBody);
//        return new ResponseEntity<String>(requestBody, HttpStatus.OK);
//    }


}
